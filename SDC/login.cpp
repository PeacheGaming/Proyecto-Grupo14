#include "login.h"
#include "ui_login.h"
#include "adminmenu.h"

Login::Login(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Login)
    {
        ui->setupUi(this);
        BD=QSqlDatabase::addDatabase("QMYSQL");
        BD.setDatabaseName("bd_carceles");
        BD.setHostName("127.0.0.1");
        BD.setPort(3306);
        BD.setUserName("root");
        BD.setPassword("123");;
        if(!BD.open()){
            QMessageBox::critical(this,"ERROR",BD.lastError().text());
            return;
        }
        tabla=new QSqlTableModel(this);
        tabla->setTable("administradores");
    }

Login::~Login()
{
    delete ui;
}

void Login::abrirMenu()
{
    tabla->setFilter("Usuario ='"+ui->txtUser->text()+"'");
    tabla->filter();
    tabla->select();
    if(tabla->rowCount()!=0){
        QString contrasenia=tabla->record(0).value(1).toString();
        if(contrasenia==ui->txtPassword->text()){
            AdminMenu *window=new AdminMenu();
            window->setModal(true);
            window->show();
        }else
           QMessageBox::critical(this,"ERROR CONTRASEÑA","CONTRASEÑA INCORRECTA");
    }else
        QMessageBox::critical(this,"ERROR USUARIO","USUARIO INCORRECTO");
}

