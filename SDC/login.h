#ifndef LOGIN_H
#define LOGIN_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlTableModel>

namespace Ui {
class Login;
}
class QSqlTableModel;
class Login : public QMainWindow
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = 0);
    ~Login();

private slots:
    void abrirMenu();

    void on_btnAcceder_clicked();

private:
    Ui::Login *ui;
    QSqlDatabase BD;
    QSqlTableModel *tabla;
};

#endif // LOGIN_H
