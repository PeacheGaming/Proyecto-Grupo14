#include "adminmenu.h"
#include "ui_adminmenu.h"

AdminMenu::AdminMenu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AdminMenu)
    {
        ui->setupUi(this);

        //Conexion Base De Datos
        BD=QSqlDatabase::addDatabase("QMYSQL");
        BD.setDatabaseName("bd_carceles");
        BD.setHostName("127.0.0.1");
        BD.setPort(3306);
        BD.setUserName("root");
        BD.setPassword("123");;
        //

        //Abrir y verificar Base de datos
        if(!BD.open()){
            QMessageBox::critical(this,"ERROR",BD.lastError().text());
            return;
        }
        //

        //Seleccionar y Llenar UbicacionBox con los departamentos
        tabla=new QSqlTableModel(this);
        tabla->setTable("departamentos");
        tabla->select();
        QSqlRecord conte;
        for (int i = 0; i < tabla->rowCount(); i++) {
             conte=tabla->record(i);
             ui->UbicacionBox->addItem(conte.value(0).toString()+"-"+conte.value(1).toString());
        }
        //
    }

AdminMenu::~AdminMenu()
{
    delete ui;
}

//FUNCIONES ADICIONALES PARA VERIFICACION DE INGRESO DE DATOS/////
bool validar_String(QString op){
    int i=0,j=op.length();
    op.resize(j);
    while(i<j){
        if(!op[i].isDigit()){
            i++;
        }else
            return false;
    }
    return true;

}
bool validar_int(QString op){
    int i=0,j=op.length();
    op.resize(j);
    while(i<j){
        if(op[i].isDigit()){
            i++;
        }else
            return false;
    }
    return true;
}
///////////////////////////////////////////////////////////////

void AdminMenu::on_pushButton_clicked()
{
    QString Error="";
    QSqlQuery query;
    query.clear();
    QString Nom=validar_String(ui->txtNombre->text())?ui->txtNombre->text():Error+="Error : Numero/s en nombre\n";
    QString Ape=validar_String(ui->txtApellidos->text())?ui->txtApellidos->text():Error+="Error : Numero/s en apellido \n";
    QString Dni=validar_int(ui->txtDNI->text())?ui->txtDNI->text():Error+="Error : Letra/s en Dni\n";

    //Verificar campos nombre y apellido si estan vacios
    if(Nom.isEmpty())
        Error+="Error: Campo Nombre vacio\n";
    if(Ape.isEmpty())
        Error+="Error: Campo Apellido vacio\n";
    //

    //Verificar Dni que sea 8 digitos
    if(Dni.length()!=8)
        Error+="Error : 8 digitos en DNI\n";
    //

    QString Sex=ui->SexoBox->currentText().split(":").at(0);
    QString Del=ui->DelitoBox->currentText();
    QString Ing=QDateTime::currentDateTime().date().toString("dd-MM-yyyy");
    QString Reclu;
    QString anio=ui->txtAnos->text();
    QString meses=ui->txtMeses->text();
    QString dias=ui->txtDias->text();


    //Si es cadena perpetua o No
    if(!ui->checkCadena->isChecked()){
        //Validar años
        if(!validar_int(anio))
            Error+="Error : Letra/s en anio\n";
        if(anio.isEmpty())
            Error+="Error: Campo anios vacio\n";
        if(anio.length()>2)
            Error+="Error: No exceder de dos digitos en campos anios\n";
        //

        //Validar meses
        if(!validar_int(meses))
            Error+="Error : Letra/s en meses\n";
        if(meses.isEmpty())
            Error+="Error: Campo meses vacio\n";
        if(meses.length()>2)
            Error+="Error: No exceder de dos digitos en campos meses\n";
        //

        //Validar dias
        if(!validar_int(dias))
            Error+="Error : Letra/s en dias\n";
        if(dias.isEmpty())
            Error+="Error: Campo dias vacio\n";
        if(dias.length()>2)
            Error+="Error: No exceder de dos digitos en campos dias\n";
        //
        Reclu=anio+"-"+meses+"-"+dias;
    }else
        Reclu="Perpetua";
    //

    QString Depar= ui->UbicacionBox->currentText().split("-").at(0);

    //Seleccionar la tabla Reo y verificar Dni
    tabla->setTable("reos");
    tabla->setFilter("Dni ="+Dni);
    tabla->filter();
    tabla->select();
    //

    //Verificar si hay reo con el dni ya ingresado
    if(tabla->rowCount()!=0)
        Error+="Error : Dni ya encontrando";
    //

    if(Error==""){
        //Seleccionar la tabla Carceles y Agarra la carcel con minima poblacion
        tabla->setTable("carceles");
        tabla->setFilter("Cod_Dep ="+Depar+" and Poblacion = (SELECT min(Poblacion) FROM Carceles WHERE Cod_Dep="+Depar+")");
        tabla->filter();
        tabla->select();
        //

        //El primer campo con poblacion minimo escogido
        QSqlRecord campo=tabla->record(0);
        QString Car=campo.value(1).toString();
        QString Cod=QString::number(campo.value(2).toInt()+1);
        //Al terminar Car = codigo de la carcel y Cod=Codigo del reo

        //Insertar en la base de datos
        query.prepare("INSERT INTO `bd_carceles`.`reos` (`Cod_dep`, `Cod(dep+car)`, `Cod_Reo`, `Nombre`, `Apellido`, `Dni`, `Sexo`, `Delito`, `Tiempo_Ingreso`, `Tiempo_Reclusion`) "
                      "VALUES ( "+Depar+","+Car+", "+Cod+", '"+Nom+"', '"+Ape+"', "+Dni+", '"+Sex+"', '"+Del+"', '"+Ing+"', '"+Reclu+"');");
        query.exec();
        //

        //Actualizar campos de la interfaz
        ui->txtNombre->setText("");
        ui->txtApellidos->setText("");
        ui->txtDNI->setText("");
        ui->txtAnos->setText("");
        ui->txtMeses->setText("");
        ui->txtDias->setText("");
        //

        //Actualizar campo , con poblacion de la carcel + 1
        campo.setValue(2,campo.value(2).toInt()+1);
        tabla->setRecord(0,campo);
        //

        QMessageBox::information(this,"REO INGRESADO","REO INGRESADO CON EXITO!!!\n\nNombre: "+Nom+
                                 "\nApellido: "+Ape+"\nDni: "+Dni+"\nDelito: "+Del+"\nCarcel:"+campo.value(3).toString()+"\nProvincia Carcel: "+campo.value(4).toString()+
                                 "\nDistrito Carcel: "+campo.value(5).toString()+"\nDireccion Carcel: "+campo.value(6).toString());
    }else
        QMessageBox::critical(this,"ERROR",Error);
}

void AdminMenu::on_checkCadena_clicked()
{
    if(ui->checkCadena->isChecked()){
        ui->txtAnos->setEnabled(false);
        ui->txtMeses->setEnabled(false);
        ui->txtDias->setEnabled(false);
    }else{
        ui->txtAnos->setEnabled(true);
        ui->txtMeses->setEnabled(true);
        ui->txtDias->setEnabled(true);
    }
}
