#-------------------------------------------------
#
# Project created by QtCreator 2016-11-06T21:13:03
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SDC
TEMPLATE = app
RC_FILE = INPE.rc
RESOURCES  = resources.qrc \
    resources.qrc


SOURCES += main.cpp\
        login.cpp \
    adminmenu.cpp

HEADERS  += login.h \
    adminmenu.h

FORMS    += login.ui \
    adminmenu.ui

RESOURCES += \
    resources.qrc

QMAKE_CXXFLAGS+= -std=gnu++11
